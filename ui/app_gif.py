
# from datetime import datetime
# import os

import streamlit as st
from keypoint import *
from module1 import *
from load_model import *
from key_on_image import *
from similarity_functions_app import *

# App Configuration
app_name ="Pose Evaluation"
PAGE_CONFIG ={"page_title":app_name,"layout":"wide"}
st.set_page_config(**PAGE_CONFIG)
config = {'displayModeBar': False, "showTips": False, 'responsive': False}

st.markdown(hide_streamlit_style, unsafe_allow_html=True)

## Streamlit Main Function

st.title("Evaluate Activity Performence")
# col1, col2, col3 = st.beta_columns((1,2,1))
# with col2 :
#     html_1 = """
#     <div style = "background-color:rgb(206.40,96);padding:10px">
#     <h1 style = "color:white;text-aline:center;">Video</h1>
#     </div>
#     """
#     st.markdown(html_1,unsafe_allow_html=True)def image_to_byte_array(image_file):
st.write("")
st.write(" It is a customised AI algorithm based System. It will Evaluate the Accuracy and Deviation of a student's performence based on Video provided by Expert and generate Report.")
st.write("")

st.subheader(":arrow_forward:  Section 1 :: Select & Play the video")
st.write("Since Expert video is fixed, Selection is done for Student's video")
uploaded_file = st.file_uploader("Upload Student's video")

if uploaded_file is None:
    st.error("Please Select Student's Video")
    st.stop()
# Load the input video
st.write(uploaded_file)
col1, col2 = st.beta_columns([1,1])
with col1 :

    st.write("Athlete's Video")
    expert_image_path = 'sample3_output.gif' #'sample3.mp4'
    image_array = image_to_byte_array(expert_image_path)
    display_image_markdown(image_array)
with col2 :

    athlete_image_path = uploaded_file.name#'./sample1.mp4'
    st.write("Student's Video")
    image_array1 = image_to_byte_array(athlete_image_path)
    display_image_markdown(image_array1)

st.header(":arrow_forward:  Section 2 :: Algorithm and Analysis ")
st.write("Here Using the AI algorithm, Student's activities pose are compaired with Expert's Activities pose. Top of that, Student's deviation score is calculated based on expert using some mathematical Formula. For Analysis purpose, the video needs to convert 'gif'.")
my_bar = st.progress(0)
for percent_complete in range(100):
    expert_image,expert_frames = information(expert_image_path)
    my_bar.progress(percent_complete + 1)

#athlete_image_path = '/home/dsights/cust_vid_analytcs/module_format/sample1.gif' 
my_bar = st.progress(0)
for percent_complete in range(100): 
    athlete_image,athlete_frames = information(athlete_image_path)
    my_bar.progress(percent_complete + 1)


with st.spinner('Please Wait for the Result ...'):
    expert_score = Run_Algorithm(movenet,init_crop_region,run_inference,expert_image)
    athlete_score = Run_Algorithm(movenet,init_crop_region,run_inference,athlete_image)
    st.success('Done!')

st.header(":arrow_forward:  Section 3 :: Evaluation Report")
st.write("The Deviation score is generated based on Expert Activities")

#Activity Completeness Status
complete_status = completeness(expert_frames, athlete_frames)
### Deviation Score Calculation
df,final_similarity_score = report(expert_score,athlete_score,expert_frames, athlete_frames)
#deviation_score(expart_frames, athlete_frames, xy,xy_athlete)
st.write("Completeness Status of Athlete based on Acitivity Attempted : " + complete_status + " %" )
st.subheader("Athlete's Deviation Score for Full Body"+ str(final_similarity_score))
st.write("The Deviation score Report of Student for Individual Body Part")
col1, col2 = st.beta_columns([1,1])
with col1 :
    st.write('Report Table for individual body Part')
    st.dataframe(df)
with col2 :
    fig = bar_plot(df,bar = 'Horizontal',title = " Deviation Score for Individual Body Part")
    st.plotly_chart(fig,config=config)


# %%
