import tensorflow as tf
import tensorflow_hub as hub
from tensorflow_docs.vis import embed
import numpy as np
import cv2

# Import matplotlib libraries
from matplotlib import pyplot as plt
from matplotlib.collections import LineCollection
import matplotlib.patches as patches

# Some modules to display an animation using imageio.
import imageio
from IPython.display import HTML, display

from keypoint import *
from module1 import *
from key_on_image import *

#pip install -q git+https://github.com/tensorflow/docs

### Load Model from TF hub
model_name = "movenet_thunder"
    #model_name = "movenet_thunder" #@param ["movenet_lightning", "movenet_thunder", "movenet_lightning.tflite", "movenet_thunder.tflite"]

# from keypoint import *if "tflite" in model_name:
#     if "movenet_lightning" in model_name:
#         !wget -q -O model.tflite https://tfhub.dev/google/lite-model/movenet/singlepose/lightning/3?lite-format=tflite
#         input_size = 192
#     elif "movenet_thunder" in model_name:
#         !wget -q -O model.tflite https://tfhub.dev/google/lite-model/movenet/singlepose/thunder/3?lite-format=tflite
#         input_size = 256
#     else:
#         raise ValueError("Unsupported model name: %s" % model_name)

#     # Initialize the TFLite interpreter
#     interpreter = tf.lite.Interpreter(model_path="model.tflite")
#     interpreter.allocate_tensors()

#     def movenet(input_image):
#         # TF Lite format expects tensor type of float32.
#         input_image = tf.cast(input_image, dtype=tf.float32)
#         input_details = interpreter.get_input_details()
#         output_details = interpreter.get_output_details()
#         interpreter.set_tensor(input_details[0]['index'], input_image.numpy())
#         # Invoke inference.
#         interpreter.invoke()
#         # Get the model prediction.
#         keypoints_with_scores = interpreter.get_tensor(output_details[0]['index'])
#         return keypoints_with_scores

# else:
if "movenet_lightning" in model_name:
    module = hub.load("https://tfhub.dev/google/movenet/singlepose/lightning/3")
    input_size = 192
elif "movenet_thunder" in model_name:
    module = hub.load("https://tfhub.dev/google/movenet/singlepose/thunder/3")
    input_size = 256
else:
    raise ValueError("Unsupported model name: %s" % model_name)

def movenet(input_image):
    model = module.signatures['serving_default']

    # SavedModel format expects tensor type of int32.
    input_image = tf.cast(input_image, dtype=tf.int32)
    # Run model inference.
    outputs = model(input_image)
    # Output is a [1, 1, 17, 3] tensor.
    keypoint_with_scores = outputs['output_0'].numpy()
    return keypoint_with_scores
