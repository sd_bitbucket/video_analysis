import streamlit as st

import plotly.figure_factory as ff
from keypoint import *
from module1 import *
from load_model import *
from key_on_image import *
from similarity_functions_app import *

# App Configuration
app_name ="Pose Evaluation"
PAGE_CONFIG ={"page_title":app_name,"layout":"wide"}
st.set_page_config(**PAGE_CONFIG)
config = {'displayModeBar': False, "showTips": False, 'responsive': True}

st.markdown(hide_streamlit_style, unsafe_allow_html=True)

#st.markdown('<font color=‘green’>THIS TEXT WILL BE RED</font>', unsafe_allow_html=True)

#st.title("Evaluate Activity Performance")
# col1, col2, col3 = st.beta_columns((1,2,1))
# with col2 :
html_1 = """
<div style = "background-color:deepskyblue;padding:10px">
<h1 style ="color:black;text-align:center;">Evaluate Activity Performance</h1>
<h4 style ="color:white;text-align:center;">It is a customised AI algorithm based System. It will Evaluate the Accuracy and Deviation of a student's performence based on Video provided by Expert and generate Report</h4>
</div>
"""
st.markdown(html_1,unsafe_allow_html=True)
st.write("")
#st.write(" It is a customised AI algorithm based System. It will Evaluate the Accuracy and Deviation of a student's performence based on Video provided by Expert and generate Report.")

st.header("Select & Play the video")
st.write("Since Expert video is fixed, Selection is done for Student's video")
uploaded_file = st.file_uploader("Upload Student's video")

if uploaded_file is None:
    st.error("Please Select Student's Video")
    st.stop()
# Load the input video
st.write(uploaded_file)
col1, col2,col3,col4 = st.beta_columns([1,1,1,1])
with col2 :
#expert_image_path =  "https://youtu.be/CkMeLrBJXiQ"  #'/home/dsights/cust_vid_analytcs/module_format/sample1.gif'
    expert_image_path = 'expert.mp4'
    video_file1 = open(expert_image_path, 'rb')
    video_bytes = video_file1.read()
    st.write("Expert's Video")
    st.video(video_bytes)
with col3 :
    #athlete_image_path = 'https://youtu.be/sN4_ovJ-Jr8' #'/home/dsights/cust_vid_analytcs/module_format/sample3.gif'
    athlete_image_path = uploaded_file.name#'./sample1.mp4'
    video_file2 = open(athlete_image_path, 'rb')
    video_bytes = video_file2.read()
    st.write("Student's Video")
    st.video(video_bytes)

st.write("") 
st.write("")     

html_2 = """
<div style = "background-color:lightskyblue;padding:10px">
<h2 style ="color:white;text-align:center;">Algorithm and Analysis</h2>
</div>
"""
st.markdown(html_2,unsafe_allow_html=True)
st.write("") 
st.write("") 
st.write("Here Using the AI algorithm, Student's activities pose are compaired with Expert's Activities pose. Top of that, Student's deviation score is calculated based on expert using some mathematical Formula. For Analysis purpose, the video needs to convert 'gif'.")
st.text('Here the converted GIF videos are used directly, Because some module is used to convert video to GIF in run time and that takes time. ')

conv = st.empty()
conv.text("Converting expert video to GIF ...")
expert_image_path = "sample3_output.gif"
my_bar = st.progress(0)
for percent_complete in range(100):
    expert_image,expert_frames = information_expert(expert_image_path)
    my_bar.progress(percent_complete + 1)
conv.text("Done")

# if st.button('Student GIF converted video'):
#     athlete_image_path = "sample1_output.gif"
conv1 = st.empty()
conv1.text("Converting athlete video to GIF ...")
my_bar = st.progress(0)
for percent_complete in range(100): 
    athlete_image,athlete_frames = information(athlete_image_path)
    my_bar.progress(percent_complete + 1)
conv1.text("Expert video conversion Done")

with st.spinner('Please Wait for the Result ... Algorithm is Running.....'):
    expert_score = Run_Algorithm(movenet,init_crop_region,run_inference,expert_image)
    athlete_score = Run_Algorithm(movenet,init_crop_region,run_inference,athlete_image)
    st.success('Athlete video conversion Done!')

st.write("")
st.write("")
#st.title(" Evaluation Report")
html_3 = """
<div style = "background-color:lightskyblue;padding:10px">
<h1 style ="color:white;text-align:center;">Evaluation Report</h1>
<h4 style ="color:black;text-align:center;">The Deviation score is generated based on Expert Activities</h4>
</div>
"""
st.markdown(html_3,unsafe_allow_html=True)
#st.text("The Deviation score is generated based on Expert Activities. ")

#Activity Completeness Status
complete_status = completeness(expert_frames, athlete_frames)
### Deviation Score Calculation
df,final_similarity_score = report(expert_score,athlete_score,expert_frames, athlete_frames)
#deviation_score(expart_frames, athlete_frames, xy,xy_athlete)
col1, col2, col3 = st.beta_columns([1,2,1])
with col2:
    st.subheader("Completeness Status of Athlete based on Acitivity Attempted : " + complete_status + " %" )
    st.header(" Athlete's Deviation Score for Full Body "+ str(final_similarity_score))
    st.subheader(" The Deviation score Report of Student for Individual Body Part")

st.write("")
st.write("")
fig,df_sort = bar_plot(df,bar = None,title = " Deviation Score for Individual Body Part")  
fig1,df_sort1 = scaled_plot(df,bar = None,title=" Scaled Deviation Score for Individual Body Part")

st.write("In the Table , the devistion score for each Individual body parts are given. Score close to 1 indicates the deviation is low (poses are similar), Score close to 0 means Poses have hight deviation.")
st.write(" And the Scaled Score is increased scale value of raw deviation score. The Scaled score value close to 100 means Poses are similar and scaled score close to 50 indicates high deviation between student and expert. ")

fig2 = ff.create_table(df_sort1)
fig2.layout.width=1730
# Make text size larger
for i in range(len(fig2.layout.annotations)):
    fig2.layout.annotations[i].font.size = 15
st.plotly_chart(fig2,config=config)

st.write("")  
st.write("") 
#st.header(" Deviation Score Comparision") 
html_4 = """
<div style = "background-color:lightskyblue;padding:10px">
<h2 style ="color:white;text-align:center;">Deviation Score Comparision</h2>
</div>
"""
st.markdown(html_4,unsafe_allow_html=True)
st.subheader(" The deviation score has been ploted to get the comparison between all individual body part.Also you can check the cooresponding score given in every bar ")
st.plotly_chart(fig,config=config)
st.write("")
st.write("")
#st.header("Rescaled Deviation Score Comparision")
html_5 = """
<div style = "background-color:lightskyblue;padding:10px">
<h2 style ="color:white;text-align:center;">Rescaled Deviation Score Comparision</h2>
</div>
"""
st.markdown(html_5,unsafe_allow_html=True)
st.subheader("The Raw Deviation Score has been rescaled and ploted to help you understand better the degree of discrimination")

#st.subheader("The following Plot represents which body parts of the student are more similar to the expert based on the Scaled deviation score. The scaled score is extend or stretch out version of Raw deviation Score.")
st.plotly_chart(fig1,config=config)

