# import tensorflow as tf
# import tensorflow_hub as hub
# #!pip install -q git+https://github.com/tensorflow/docs
# from tensorflow_docs.vis import embed
# import numpy as np
# import cv2
# import math
# from moviepy.editor import *

# # Import matplotlib libraries
# from matplotlib import pyplot as plt
# from matplotlib.collections import LineCollection
# import matplotlib.patches as patches

# # Some modules to display an animation using imageio.
# import imageio
# from IPython.display import HTML, display

# import sys
# if not sys.warnoptions:
#     import warnings
#     warnings.simplefilter("ignore")

# import subprocess
# import sys

# def install(package):
#     subprocess.check_call([sys.executable, "-m", "pip", "install", package])
# install(git+https://github.com/tensorflow/docs)

# from datetime import datetime
# import os

import streamlit as st
from keypoint import *
from module1 import *
from load_model import *
from key_on_image import *
from similarity_functions_app import *

## Streamlit Main Function

st.title("Evaluate Activity Performence")
# col1, col2, col3 = st.beta_columns((1,2,1))
# with col2 :
#     html_1 = """
#     <div style = "background-color:rgb(206.40,96);padding:10px">
#     <h1 style = "color:white;text-aline:center;">Video</h1>
#     </div>
#     """
#     st.markdown(html_1,unsafe_allow_html=True)
st.write("")
st.write(" It is a customised AI algorithm based System. It will Evaluate the Accuracy and Deviation of a student's performence based on Video provided by Expert and generate Report.")
st.write("")

st.subheader(":arrow_forward:  Section 1 :: Select & Play the video")

uploaded_file = st.file_uploader("Upload file")

if uploaded_file is None:
    st.error("Please Select Student's Video")
    st.stop()
# Load the input video
st.write(uploaded_file)
col1, col2 = st.beta_columns([1,1])
with col1 :
#expert_image_path =  "https://youtu.be/CkMeLrBJXiQ"  #'/home/dsights/cust_vid_analytcs/module_format/sample1.gif'
    expert_image_path = 'sample3.mp4'
    video_file1 = open(expert_image_path, 'rb')
    video_bytes = video_file1.read()
    st.write("Expert's Video")
    st.video(video_bytes)
with col2 :
    #athlete_image_path = 'https://youtu.be/sN4_ovJ-Jr8' #'/home/dsights/cust_vid_analytcs/module_format/sample3.gif'
    athlete_image_path = uploaded_file.name#'./sample1.mp4'
    video_file2 = open(athlete_image_path, 'rb')
    video_bytes = video_file2.read()
    st.write("Student's Video")
    st.video(video_bytes)

st.header(":arrow_forward:  Section 2 :: Algorithm and Analysis ")
st.write("Here Using the AI algorithm, Student's activities pose are compaired with Expert's Activities pose. Top of that, Student's deviation score is calculated based on expert using some mathematical Formula.")
#This Section for Expart Video
#expert_image_path = '/home/dsights/cust_vid_analytcs/module_format/sample3.gif'
my_bar = st.progress(0)
for percent_complete in range(100):
    expert_image,expert_frames,expert_inform = information(expert_image_path)
    my_bar.progress(percent_complete + 1)

#athlete_image_path = '/home/dsights/cust_vid_analytcs/module_format/sample1.gif' 
my_bar = st.progress(0)
for percent_complete in range(100): 
    athlete_image,athlete_frames,athlete_inform = information(athlete_image_path)
    my_bar.progress(percent_complete + 1)


with st.spinner('Please Wait for the Result ...'):
    expert_score = Run_Algorithm(movenet,init_crop_region,run_inference,expert_image)
    
#This Section for Athlete's Video
#athlete_image,athlete_frames,athlete_inform = information(athlete_image_path)
#st.text(athlete_inform)
    athlete_score = Run_Algorithm(movenet,init_crop_region,run_inference,athlete_image)
    st.success('Done!')

st.header(":arrow_forward:  Section 3 :: Evaluation Report")
st.write("The Deviation score Report of Student for Individual Body Part")
#Activity Completeness Status
complete_status = completeness(expert_frames, athlete_frames)
### Deviation Score Calculation
df = report(expert_score,athlete_score,expert_frames, athlete_frames)
#deviation_score(expart_frames, athlete_frames, xy,xy_athlete)
st.text(complete_status)

# col1, col2 = st.beta_columns([1,1])
# with col1 :
st.write('Report Table for individual body Part')
st.dataframe(df)

fig = bar_plot(df,bar = 'Horizontal',title = " Deviation Score for Individual Body Part")
st.plotly_chart(fig)


# %%
