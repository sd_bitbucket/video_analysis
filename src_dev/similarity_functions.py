import tensorflow as tf
import tensorflow_hub as hub
from tensorflow_docs.vis import embed
import pandas as pd
import numpy as np
import cv2
import math
from moviepy.editor import *
import random
from datetime import datetime
import os

import plotly.graph_objects as go

# Import matplotlib libraries
from matplotlib import pyplot as plt
from matplotlib.collections import LineCollection
import matplotlib.patches as patches

# Some modules to display an animation using imageio.
import imageio
from IPython.display import HTML, display
from numpy import dot
from numpy.linalg import norm

import sys
if not sys.warnoptions:
    import warnings
    warnings.simplefilter("ignore")


from keypoint import *
from module1 import *
from load_model import *


def open_video(image_path):
    cap = cv2.VideoCapture(image_path)
    if (cap.isOpened()== False): 
      print("Error opening video  file")
    while(cap.isOpened()):
      ret, frame = cap.read()
      if ret == True:
        cv2.imshow('Frame', frame)
        if cv2.waitKey(25) & 0xFF == ord('q'):
          break
      else: 
        break
    cap.release()
    cv2.destroyAllWindows()


def about_raw_video(image_path):
    video = cv2.VideoCapture(image_path)
    print("Duration of the video  :: " + str(VideoFileClip(image_path).duration) +" Second" )
    print ("Frames per second :" ,math.ceil(cv2.VideoCapture(image_path).get(cv2.CAP_PROP_FPS)))
    
    print("Number of Frames of this video : ", math.ceil(video.get(cv2.CAP_PROP_FRAME_COUNT)))
    print("Height and Width of Frame : " + str(math.ceil(video.get(cv2.CAP_PROP_FRAME_WIDTH)))+" pixels & " 
          +str(math.ceil(video.get(cv2.CAP_PROP_FRAME_HEIGHT ))) +" pixels")
    return math.ceil(video.get(cv2.CAP_PROP_FRAME_COUNT))

def about_video(gif_file,image):
    print("Duration of the video  :: " + str(VideoFileClip(gif_file).duration) +" Second" )
    print ("Frames per second :" ,math.ceil(cv2.VideoCapture(gif_file).get(cv2.CAP_PROP_FPS)))
    print("Number of Frames of this video : ", image.shape[0])
    print("Height and Width of the frame : " + str(image.shape[1])+" pixels & " +str(image.shape[2]) +" pixels")
    
    
def Run_Algorithm(movenet,init_crop_region,run_inference,image):
    output_images = []
    score=[]
    
    num_frames, image_height, image_width, _ = image.shape
    crop_region = init_crop_region(image_height, image_width)
    bar = display(progress(0, num_frames-1), display_id=True)
    for frame_idx in range(num_frames):
        keypoints_with_scores = run_inference(movenet, image[frame_idx, :, :, :], crop_region,crop_size=[input_size, 
                                                                                                         input_size])
        score.append(keypoints_with_scores)
        bar.update(progress(frame_idx, num_frames-1))
    
    #xy,predict_score,less_score_frame = coordinate(score,num_frames,body_parts_name)
        
    return score     
    
    
def information(image_path):
    print("\033[1m" + "Information about Raw Video" + "\033[0m")
    about_raw_video(image_path)
    
    print(" ")
    print(" ")
    print("Converting ...")
    gif_file= str(image_path.split('.')[0])+'_output'+'.gif'
    clip = (VideoFileClip(image_path))
    clip.write_gif(gif_file)
    print(" Done ")
    
    image = tf.io.read_file(gif_file)
    image = tf.image.decode_gif(image)
    print(" ")
    print(" ")
    print("\033[1m" + "Information about converted (gif) Video" + "\033[0m")
    about_video(gif_file,image)
    num_frames, image_height, image_width, _ = image.shape
    return image,num_frames    
    
      

def coordinate(score,num_frames,body_parts_name = 'nose'):
    
    body_key_point = KEYPOINT_DICT[body_parts_name] 
    
    less_score_frame  =[]
    predict_score=[]
    y_axis=[]
    x_axis=[]
    for i in range(num_frames):
        y = score[i][0,0,body_key_point,0]
        y_axis.append(y) 
        x = score[i][0,0,body_key_point,1]
        x_axis.append(x) 

        pred_score = score[i][0,0,body_key_point,2]
        predict_score.append(pred_score)
        if pred_score <= 0.35:
            less_score_frame.append(i)
        xy=[]
        xy.extend([list(a) for a in zip(x_axis,y_axis)])    
    return xy,predict_score,less_score_frame      


# def deviation_score(expart_frames, athlete_frames, xy,xy_athlete, body_parts_name):
#     body_key_point = KEYPOINT_DICT[body_parts_name]
#     div_score=[]
#     if expart_frames <= athlete_frames :
#         for f in range(expart_frames):
#             #print(xy[f])
#             sim_score = dot(xy[f],xy_athlete[f])/(norm(xy[f])*norm(xy_athlete[f]))
#             #print("Simi score is "+str(round(sim_score,2))+ "for frame "+ str(f))
#             div_score.append(sim_score) 
#         print("Athlete's Final Score for "+ "\033[1m" + body_parts_name + "\033[0m" +" :: "+ str(round(sum(div_score) / len(div_score),2) ))   
#     else:
#         random_frame= sorted(random.sample(range(expart_frames), athlete_frames))
#         for i,k in zip(random_frame,range(athlete_frames)):
#             sim_score = dot(xy[i],xy_athlete[k])/(norm(xy[i])*norm(xy_athlete[k]))
#             #print("Simi score is "+str(round(sim_score,2))+ "for frame "+ str(f))
#             div_score.append(sim_score) 
#             #print (str(i), ',', str(k))
#         print("Athlete's Final Score for "+ "\033[1m" + body_parts_name + "\033[0m" +" :: "+ str(round(sum(div_score) / len(div_score),2) ))   
#     return sim_score   
        
def completeness(expart_frames, athlete_frames):
    if expart_frames < athlete_frames :
        print( "\033[1m" + "Completeness Status " + "\033[0m" + "of Athlete based on Acitivity Attempted : 100 %")
    else :    
        com_status = int((athlete_frames/expart_frames)*100)
        print( "\033[1m" + "Completeness Status " + "\033[0m" + "of Athlete based on Acitivity Attempted :", str(com_status )+ " %")  
        
#######
def deviation_score(expart_frames, athlete_frames, xy,xy_athlete, body_parts_name):
    body_key_point = KEYPOINT_DICT[body_parts_name]
    div_score=[]
    if expart_frames <= athlete_frames :
        for f in range(expart_frames):
            #print(xy[f])
            sim_score = dot(xy[f],xy_athlete[f])/((norm(xy[f])*norm(xy_athlete[f])))
            #print("Simi score is "+str(round(sim_score,2))+ "for frame "+ str(f))
            div_score.append(sim_score) 
           
    else:
        random_frame= sorted(random.sample(range(expart_frames), athlete_frames))
        for i,k in zip(random_frame,range(athlete_frames)):
            sim_score = dot(xy[i],xy_athlete[k])/((norm(xy[i])*norm(xy_athlete[k])))
            #print("Simi score is "+str(round(sim_score,2))+ "for frame "+ str(f))
            div_score.append(sim_score) 
          
    return round(sum(div_score) / len(div_score),4) 
        
    
    
####### Result Dataframe    
def report(expert_score,athlete_score,expart_frames, athlete_frames):
    all_body_part = list(KEYPOINT_DICT.keys())
    body_key_point=[]
    similar_score = []
    status=[]
    weight=[]
    for p in all_body_part :
        xy,predict_score,less_score_frame = coordinate(expert_score,num_frames =expart_frames,body_parts_name = p)
        xy_athlete,predict_score_athlete,less_score_frame_athlete = coordinate(athlete_score,num_frames =athlete_frames,body_parts_name = p)
        sim_score = deviation_score(expart_frames, athlete_frames, xy,xy_athlete, body_parts_name=p)
   
        similar_score.append(sim_score)
        body_key_point.append(p)

    weight=[round(100/18,2)]*18 
    if expart_frames < athlete_frames :
        com_status = 1 
    else :    
        com_status = round((athlete_frames/expart_frames),4)
        
    body_key_point.append('Completeness') 
    similar_score.append(com_status)
    report_df =pd.DataFrame()
    report_df['body_key_point']=body_key_point
    report_df['deviation_score']=similar_score
    report_df['weight_for_body_point (%)']=weight
    
    weighted = [(weight[i]/100) * similar_score[i] for i in range(len(weight))]
    report_df['Final Similarity Score']= [(round(sum(weighted),4))*100] + ['']*17 
    
    
    print("\033[1m" + "The result is based on the assumption that the Frame Rate Per Second (fps) of Two video should be same (30fps) " + "\033[0m")
    return report_df


###################
## Bar  Plot
def bar_plot(data,bar = 'Horizontal',title='Plot Title'):
    bars = []
    
    df_sort = data.sort_values(by=['deviation_score'],ascending=False)
    x = df_sort['body_key_point']
    y = df_sort['deviation_score']
    df_sort['label']=[None]*18

    for b in range(18):
        if df_sort['deviation_score'][b] >= 0.99:
            df_sort['label'][b] = 'green'
        if (df_sort['deviation_score'][b] <= 0.99) & (df_sort['deviation_score'][b] >= 0.98) :
            df_sort['label'][b] = "orange"
        if df_sort['deviation_score'][b] <= 0.98 :
            df_sort['label'][b] = "red"
    
    if bar == 'Horizontal':
        for label, label_df in df_sort.groupby('label'):
            bars.append(go.Bar(x=label_df.deviation_score,
                           y=label_df.body_key_point ,
                           name=label,
                           text= label_df.deviation_score,
                           marker={'color': label},
                           orientation='h',
                          ))
    else :
        for label, label_df in df_sort.groupby('label'):
            bars.append(go.Bar(x=label_df.body_key_point,
                               y=label_df.deviation_score,
                               name=label,
                               marker={'color': label},
                               text= label_df.deviation_score,
                               textposition='auto',
                              ))

    fig = go.FigureWidget(data=bars)

    fig.update_layout(barmode='group',xaxis=dict(title_text="Body Part"),yaxis=dict(title_text="Deviation Score"),
                      title_text=title,title_x=0.5)
    fig.update_layout(width =800,height=700,autosize=True,)
    return fig.show()

#######
def scatter_plot(x,y,x1,y1,line_1 = 'line_1',line_2= 'line_2',title='Plot Title',):
    
    trace_1=go.Scatter(x=x,y=y,line=dict(color="rgb(206,40,94)",width=2),mode="markers",
                   name=line_1)
    trace_2=go.Scatter(x=x1,y=y1,line=dict(color="rgb(89,89,89)",width=2),mode="markers",
                   name=line_2)
    
    layout=go.Layout(legend_orientation="h",xaxis=dict(title_text="x-axis"),yaxis=dict(title_text="y-axis"),
                 title_text=title,title_x=0.5,title_y=0.9)
    fig=go.Figure(data=[trace_1,trace_2],layout=layout)

    fig.update_layout(showlegend=True,width=600,height=600,autosize=True,
        )
    return fig.show()

############
def xy_compare(expert_score,athlete_score,expart_frames,athlete_frames,body_parts_name = 'left_shoulder'):
    expert_xy,_,_ = coordinate(expert_score,expart_frames,body_parts_name = body_parts_name)
    athlete_xy,_,_ = coordinate(athlete_score,athlete_frames,body_parts_name = body_parts_name)
    
    df_xy = pd.DataFrame(list(zip(expert_xy, athlete_xy[0:len(expert_xy)])),
                   columns =['expert', 'athlete'])
    df_xy #.to_csv("coordinates_left_wrist.csv",index=False)
    expert_x =[]
    expert_y =[]
    athlete_x =[]
    athlete_y =[]
    for f in range(expart_frames):
        expert_x.append(df_xy['expert'][f][0])
        expert_y.append(df_xy['expert'][f][1])
        athlete_x.append(df_xy['athlete'][f][0])
        athlete_y.append(df_xy['athlete'][f][1])
    scatter_plot(expert_x,expert_y,athlete_x,athlete_y,line_1 = 'expert',line_2= 'athlete',
                          title='Coordinate Comparision for '+ body_parts_name)
    
    return None