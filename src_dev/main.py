import tensorflow as tf
import tensorflow_hub as hub
#!pip install -q git+https://github.com/tensorflow/docs
from tensorflow_docs.vis import embed
import numpy as np
import cv2
import math
from moviepy.editor import *

# Import matplotlib libraries
from matplotlib import pyplot as plt
from matplotlib.collections import LineCollection
import matplotlib.patches as patches

# Some modules to display an animation using imageio.
import imageio
from IPython.display import HTML, display

import sys
if not sys.warnoptions:
    import warnings
    warnings.simplefilter("ignore")

import subprocess
import sys

# def install(package):
#     subprocess.check_call([sys.executable, "-m", "pip", "install", package])
# install(git+https://github.com/tensorflow/docs)

from datetime import datetime
import os
from keypoint import *
from module1 import *
from load_model import *
from key_on_image import *
from similarity_functions_app import *


# Load the input video
expart_image_path = '/home/dsights/cust_vid_analytcs/sample1.mp4'
athlete_image_path = '/home/dsights/cust_vid_analytcs/sample3.mp4'

#This Section for Expart Video
expart_image,expart_frames = information(expart_image_path)
expert_score = Run_Algorithm(movenet,init_crop_region,run_inference,expart_image)

#This Section for Athlete's Video
athlete_image,athlete_frames = information(athlete_image_path)
athlete_score = Run_Algorithm(movenet,init_crop_region,run_inference,athlete_image)

#Activity Completeness Status
completeness(expart_frames, athlete_frames)
### Deviation Score Calculation
report(expert_score,athlete_score,expart_frames, athlete_frames)
#deviation_score(expart_frames, athlete_frames, xy,xy_athlete)