import tensorflow as tf
import tensorflow_hub as hub
from tensorflow_docs.vis import embed
import numpy as np
import cv2
import math
from moviepy.editor import *
import random

# Import matplotlib libraries
from matplotlib import pyplot as plt
from matplotlib.collections import LineCollection
import matplotlib.patches as patches

# Some modules to display an animation using imageio.
import imageio
from IPython.display import HTML, display
from numpy import dot
from numpy.linalg import norm

import sys
if not sys.warnoptions:
    import warnings
    warnings.simplefilter("ignore")


from keypoint import *
from module1 import *
from load_model import *


def open_video(image_path):
    cap = cv2.VideoCapture(image_path)
    if (cap.isOpened()== False): 
      print("Error opening video  file")
    while(cap.isOpened()):
      ret, frame = cap.read()
      if ret == True:
        cv2.imshow('Frame', frame)
        if cv2.waitKey(25) & 0xFF == ord('q'):
          break
      else: 
        break
    cap.release()
    cv2.destroyAllWindows()


def about_raw_video(image_path):
    video = cv2.VideoCapture(image_path)
    print("Duration of the video  :: " + str(VideoFileClip(image_path).duration) +" Second" )
    print ("Frames per second :" ,math.ceil(cv2.VideoCapture(image_path).get(cv2.CAP_PROP_FPS)))
    
    print("Number of Frames of this video : ", math.ceil(video.get(cv2.CAP_PROP_FRAME_COUNT)))
    print("Height and Width of Frame : " + str(math.ceil(video.get(cv2.CAP_PROP_FRAME_WIDTH)))+" pixels & " 
          +str(math.ceil(video.get(cv2.CAP_PROP_FRAME_HEIGHT ))) +" pixels")
    return math.ceil(video.get(cv2.CAP_PROP_FRAME_COUNT))

def about_video(gif_file,image):
    print("Duration of the video  :: " + str(VideoFileClip(gif_file).duration) +" Second" )
    print ("Frames per second :" ,math.ceil(cv2.VideoCapture(gif_file).get(cv2.CAP_PROP_FPS)))
    print("Number of Frames of this video : ", image.shape[0])
    print("Height and Width of the frame : " + str(image.shape[1])+" pixels & " +str(image.shape[2]) +" pixels")
    
    
def Run_Algorithm(movenet,init_crop_region,run_inference,image,body_parts_name):
    output_images = []
    score=[]
    
    num_frames, image_height, image_width, _ = image.shape
    crop_region = init_crop_region(image_height, image_width)
    bar = display(progress(0, num_frames-1), display_id=True)
    for frame_idx in range(num_frames):
        keypoints_with_scores = run_inference(movenet, image[frame_idx, :, :, :], crop_region,crop_size=[input_size, 
                                                                                                         input_size])
        score.append(keypoints_with_scores)
        #bar.update(progress(frame_idx, num_frames-1))
    
    xy,predict_score,less_score_frame = coordinate(score,num_frames,body_parts_name)
        
    return xy,predict_score,less_score_frame     
    
    
def information(image_path):
    print("\033[1m" + "Information about Raw Video" + "\033[0m")
    about_raw_video(image_path)
    
    print(" ")
    print(" ")
    print("Converting ...")
    gif_file= str(image_path.split('.')[0])+'_output'+'.gif'
    clip = (VideoFileClip(image_path))
    clip.write_gif(gif_file)
    print(" Done ")
    
    image = tf.io.read_file(gif_file)
    image = tf.image.decode_gif(image)
    print(" ")
    print(" ")
    print("\033[1m" + "Information about converted (gif) Video" + "\033[0m")
    about_video(gif_file,image)
    num_frames, image_height, image_width, _ = image.shape
    return image,num_frames    
    
      

def coordinate(score,num_frames,body_parts_name = 'nose'):
    
    body_key_point = KEYPOINT_DICT[body_parts_name] 
    
    less_score_frame  =[]
    predict_score=[]
    y_axis=[]
    x_axis=[]
    for i in range(num_frames):
        y = score[i][0,0,body_key_point,0]
        y_axis.append(y) 
        x = score[i][0,0,body_key_point,1]
        x_axis.append(x) 

        pred_score = score[i][0,0,body_key_point,2]
        predict_score.append(pred_score)
        if pred_score <= 0.35:
            less_score_frame.append(i)
        xy=[]
        xy.extend([list(a) for a in zip(x_axis,y_axis)])    
    return xy,predict_score,less_score_frame      


def deviation_score(expart_frames, athlete_frames, xy,xy_athlete, body_parts_name):
    body_key_point = KEYPOINT_DICT[body_parts_name]
    div_score=[]
    if expart_frames <= athlete_frames :
        for f in range(expart_frames):
            #print(xy[f])
            sim_score = dot(xy[f],xy_athlete[f])/(norm(xy[f])*norm(xy_athlete[f]))
            #print("Simi score is "+str(round(sim_score,2))+ "for frame "+ str(f))
            div_score.append(sim_score) 
        print("Athlete's Final Score for "+ "\033[1m" + body_parts_name + "\033[0m" +" :: "+ str(round(sum(div_score) / len(div_score),2) ))   
    else:
        random_frame= sorted(random.sample(range(expart_frames), athlete_frames))
        for i,k in zip(random_frame,range(athlete_frames)):
            sim_score = dot(xy[i],xy_athlete[k])/(norm(xy[i])*norm(xy_athlete[k]))
            #print("Simi score is "+str(round(sim_score,2))+ "for frame "+ str(f))
            div_score.append(sim_score) 
            #print (str(i), ',', str(k))
        print("Athlete's Final Score for "+ "\033[1m" + body_parts_name + "\033[0m" +" :: "+ str(round(sum(div_score) / len(div_score),2) ))   
        
        
def completeness(expart_frames, athlete_frames):
    com_status = int((athlete_frames/expart_frames)*100)
    print( "\033[1m" + "Completeness Status " + "\033[0m" + "of Athlete based on Acitivity Attempted :", str(com_status )+ " %")     
    
    
    
    
    
### Combination,  10c2 

# import itertools

# frm=[]
# for comb in itertools.combinations(range(10), 2):
#     #print (comb)
#     frm.append(comb)    