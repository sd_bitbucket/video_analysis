import tensorflow as tf
import tensorflow_hub as hub
from tensorflow_docs.vis import embed
import pandas as pd
import numpy as np
import cv2
import math
from moviepy.editor import *
import random
from datetime import datetime
import os

import plotly.graph_objects as go

# Import matplotlib libraries
from matplotlib import pyplot as plt
from matplotlib.collections import LineCollection
import matplotlib.patches as patches

# Some modules to display an animation using imageio.
import imageio
from IPython.display import HTML, display
from numpy import dot
from numpy.linalg import norm

import sys
if not sys.warnoptions:
    import warnings
    warnings.simplefilter("ignore")

import streamlit as st
from keypoint import *
from module1 import *
from load_model import *

from google.cloud import storage


hide_streamlit_style = """
<style>
#MainMenu {visibility: hidden;}
footer {visibility: hidden;}
footer:after {
	content:'www.dsightsonline.com'; 
	visibility: visible;
	display: block;
	position: relative;
	#background-color: red;
	padding: 5px;
	top: 2px;
}
</style>
"""

def upload_blob(bucket_name, source_file_name, destination_blob_name):
    """Uploads a file to the bucket."""
    # The ID of your GCS bucket
    # bucket_name = "your-bucket-name"
    # The path to your file to upload
    # source_file_name = "local/path/to/file"
    # The ID of your GCS object
    # destination_blob_name = "storage-object-name"

    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(destination_blob_name)

    blob.upload_from_filename(source_file_name)


def download_blob(bucket_name, source_blob_name, destination_file_name):
    """Downloads a blob from the bucket."""
    # bucket_name = "your-bucket-name"
    # source_blob_name = "storage-object-name"
    # destination_file_name = "local/path/to/file"

    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(source_blob_name)
    blob.download_to_filename(destination_file_name)    


def about_raw_video(image_path):
    video = cv2.VideoCapture(image_path)
    print("Duration of the video  :: " + str(VideoFileClip(image_path).duration) +" Second" )
    print ("Frames per second :" ,math.ceil(cv2.VideoCapture(image_path).get(cv2.CAP_PROP_FPS)))
    
    print("Number of Frames of this video : ", math.ceil(video.get(cv2.CAP_PROP_FRAME_COUNT)))
    print("Height and Width of Frame : " + str(math.ceil(video.get(cv2.CAP_PROP_FRAME_WIDTH)))+" pixels & " 
          +str(math.ceil(video.get(cv2.CAP_PROP_FRAME_HEIGHT ))) +" pixels")
    return math.ceil(video.get(cv2.CAP_PROP_FRAME_COUNT))

def about_video(gif_file,image):
    print("Duration of the video  :: " + str(VideoFileClip(gif_file).duration) +" Second" )
    print ("Frames per second :" ,math.ceil(cv2.VideoCapture(gif_file).get(cv2.CAP_PROP_FPS)))
    print("Number of Frames of this video : ", image.shape[0])
    #print("Height and Width of the frame : " + str(image.shape[1])+" pixels & " +str(image.shape[2]) +" pixels")
    
    
def Run_Algorithm(movenet,init_crop_region,run_inference,image):
    output_images = []
    score=[]
    
    num_frames, image_height, image_width, _ = image.shape
    crop_region = init_crop_region(image_height, image_width)
    bar = display(progress(0, num_frames-1), display_id=True)
    for frame_idx in range(num_frames):
        keypoints_with_scores = run_inference(movenet, image[frame_idx, :, :, :], crop_region,crop_size=[input_size, 
                                                                                                         input_size])
        score.append(keypoints_with_scores)
        #bar.update(progress(frame_idx, num_frames-1))
    
    #xy,predict_score,less_score_frame = coordinate(score,num_frames,body_parts_name)
        
    return score  
#### For expert Video Read and convert
@st.cache 
def information_expert(image_path):
    print("\033[1m" + "Information about Raw Video" + "\033[0m")
    about_raw_video(image_path)
    
    print(" ")
    print(" ")
    # print("Converting ...")
    # gif_file= str(image_path.split('.')[0])+'_output'+'.gif'
    # clip = (VideoFileClip(image_path))
    # clip.write_gif(gif_file)
    # print(" Done ")
    
    image = tf.io.read_file(image_path)
    image = tf.image.decode_gif(image)
    # print(" ")
    # print(" ")
    # print("\033[1m" + "Information about converted (gif) Video" + "\033[0m")
    # about_video(gif_file,image)
    num_frames, image_height, image_width, _ = image.shape
    return image,num_frames     
           
##### For athlete Video Read and convert    
@st.cache 
def information(image_path):
    print("\033[1m" + "Information about Raw Video" + "\033[0m")
    about_raw_video(image_path)
    
    print(" ")
    print(" ")
    print("Converting ...")
    gif_file= str(image_path.split('.')[0])+'_output'+'.gif'
    clip = (VideoFileClip(image_path))
    clip.write_gif(gif_file)
    print(" Done ")
    
    image = tf.io.read_file(gif_file)
    image = tf.image.decode_gif(image)
    print(" ")
    print(" ")
    print("\033[1m" + "Information about converted (gif) Video" + "\033[0m")
    about_video(gif_file,image)
    num_frames, image_height, image_width, _ = image.shape
    return image,num_frames     
    
      

def coordinate(score,num_frames,body_parts_name = 'nose'):
    
    body_key_point = KEYPOINT_DICT[body_parts_name] 
    
    less_score_frame  =[]
    predict_score=[]
    y_axis=[]
    x_axis=[]
    for i in range(num_frames):
        y = score[i][0,0,body_key_point,0]
        y_axis.append(y) 
        x = score[i][0,0,body_key_point,1]
        x_axis.append(x) 

        pred_score = score[i][0,0,body_key_point,2]
        predict_score.append(pred_score)
        if pred_score <= 0.35:
            less_score_frame.append(i)
        xy=[]
        xy.extend([list(a) for a in zip(x_axis,y_axis)])    
    return xy,predict_score,less_score_frame      
  
        
def completeness(expart_frames, athlete_frames):
    if expart_frames < athlete_frames :
        print( "\033[1m" + "Completeness Status " + "\033[0m" + "of Athlete based on Acitivity Attempted : 100 %")
        text =  100 
    else :    
        com_status = int((athlete_frames/expart_frames)*100)
        print( "\033[1m" + "Completeness Status " + "\033[0m" + "of Athlete based on Acitivity Attempted :", str(com_status )+ " %") 
        text = str(com_status )

    status=pd.DataFrame([com_status],columns=['com_status'])
    #os.mkdir("output")
    csv_file = 'completness_status_' + str(datetime.now().strftime('%Y_%m_%d')) + '.csv'  #_%H_%M_%S
    status.to_csv("./output/"+csv_file,index=False)

    return text     
#######
def deviation_score(expart_frames, athlete_frames, xy,xy_athlete, body_parts_name):
    body_key_point = KEYPOINT_DICT[body_parts_name]
    div_score=[]
    if expart_frames <= athlete_frames :
        for f in range(expart_frames):
            #print(xy[f])
            sim_score = dot(xy[f],xy_athlete[f])/((norm(xy[f])*norm(xy_athlete[f])))
            #print("Simi score is "+str(round(sim_score,2))+ "for frame "+ str(f))
            div_score.append(sim_score) 
           
    else:
        random_frame= sorted(random.sample(range(expart_frames), athlete_frames))
        for i,k in zip(random_frame,range(athlete_frames)):
            sim_score = dot(xy[i],xy_athlete[k])/((norm(xy[i])*norm(xy_athlete[k])))
            #print("Simi score is "+str(round(sim_score,2))+ "for frame "+ str(f))
            div_score.append(sim_score) 
          
    return round(sum(div_score) / len(div_score),4) 
        
    st.text("Converting to GIF ...")
   
####### Result Dataframe 
body_point = ['Nose','Left Eye','Right Eye','Left Ear','Right Ear','Left Shoulder','Right Shoulder','Left Elbow','Right Elbow','Left Wrist','Right Wrist','Left Hip','Right Hip',
'Left Knee','Right Knee','Left Ankle','Right Ankle']

def report(expert_score,athlete_score,expart_frames, athlete_frames):
    all_body_part = list(KEYPOINT_DICT.keys())
    body_key_point=[]
    similar_score = []
    status=[]
    weight=[]
    body_point = ['Nose','Left Eye','Right Eye','Left Ear','Right Ear','Left Shoulder','Right Shoulder','Left Elbow','Right Elbow','Left Wrist','Right Wrist','Left Hip','Right Hip',
                    'Left Knee','Right Knee','Left Ankle','Right Ankle']

    for p in all_body_part :
        xy,predict_score,less_score_frame = coordinate(expert_score,num_frames =expart_frames,body_parts_name = p)
        xy_athlete,predict_score_athlete,less_score_frame_athlete = coordinate(athlete_score,num_frames =athlete_frames,body_parts_name = p)
        sim_score = deviation_score(expart_frames, athlete_frames, xy,xy_athlete, body_parts_name=p)
   
        similar_score.append(sim_score)
        body_key_point.append(p)

    weight=[round(100/18,2)]*18 
    if expart_frames < athlete_frames :
        com_status = 1 
    else :    
        com_status = round((athlete_frames/expart_frames),4)
        
    body_point.append('Completeness') 
    similar_score.append(com_status)
    report_df =pd.DataFrame()
    report_df['body_key_point']=body_point
    report_df['deviation_score']=similar_score
    #report_df['weight_for_body_point (%)']=weight
    
    weighted = [(weight[i]/100) * similar_score[i] for i in range(len(weight))]
    #report_df['Final Similarity Score']= [(round(sum(weighted),4))*100] + ['']*17 
    final_similarity_score = round(sum(weighted)*100,2)

    status1=pd.DataFrame([final_similarity_score],columns=['final_similarity_score'])
    csv_file1 = 'deviation_score_full_' + str(datetime.now().strftime('%Y_%m_%d')) + '.csv'  #_%H_%M_%S
    status1.to_csv("./output/"+csv_file1,index=False)
    
    print("\033[1m" + "The result is based on the assumption that the Frame Rate Per Second (fps) of Two video should be same (30fps) " + "\033[0m")
    return report_df,final_similarity_score


###################
## Bar  Plot
def bar_plot(data,bar = 'Horizontal',title='Plot Title'):
    bars = []

    df_sort = data.sort_values(by=['deviation_score'],ascending=False).reset_index(drop=True)
    x = df_sort['body_key_point']
    y = df_sort['deviation_score']
    df_sort['label']=[None]*18
    df_sort['col']=[None]*18

    for b in range(18):
        if df_sort['deviation_score'][b] > 0.99:  #0.99
        #if c[b] >= 50:  #0.99
            df_sort['label'][b] = '1. Great Performance'
            df_sort['col'][b] = 'mediumspringgreen'            
        if (df_sort['deviation_score'][b] <= 0.99) & (df_sort['deviation_score'][b] > 0.98) :
        #if (c[b] <= 50) & (c[b] >= 15) :   
            df_sort['label'][b] = "2. Good Performance"
            df_sort['col'][b] = 'gold'
        if df_sort['deviation_score'][b] <= 0.98 :
        #if c[b] <= 0.15 :
            df_sort['label'][b] = "3. Needs Improvement"
            df_sort['col'][b] = 'tomato'

    if bar == 'Horizontal':
        for label, label_df in df_sort.groupby('label'):
            bars.append(go.Bar(x=label_df.deviation_score,
                           y=(label_df.body_key_point) ,
                           name=label,
                           text= label_df.deviation_score,
                           marker={'color': label_df.col},
                           orientation='h',
                          ))
    else :
        for label, label_df in df_sort.groupby('label'):
            bars.append(go.Bar(x=label_df.body_key_point,
                               y=label_df.deviation_score,
                               name=label,
                               marker={'color': label_df.col},
                               text= label_df.deviation_score,
                               textposition='auto',
                              ))

    
    fig = go.FigureWidget(data=bars#,layout=layout
                         )

    fig.update_layout(barmode='group',xaxis=dict(title_text="Body Part"),yaxis=dict(title_text="Deviation Score"),
                      title_text=title,title_x=0.5,
                  
                     )
    fig.update_layout(legend_orientation="h",autosize=True,width =1760,height=530,
                        title_font_color="red",
                        legend_font_color="blue", 
                        font=dict(size=18,
                     )) 
    fig.update_layout(legend=dict(
    #yanchor="top",
    y=-.23,
    #xanchor="left",
    x=0.01
    ))
    fig.update_xaxes(tickangle=335)
    
    return fig,df_sort[['body_key_point','deviation_score','label' ]]

######
def scaled_plot(data,bar = 'Horizontal',title='Plot Title'):
    bars = []

    df_sort = data.sort_values(by=['deviation_score'],ascending=False).reset_index(drop=True)
    x = df_sort['body_key_point']
    y = df_sort['deviation_score']

    OldRange = (1-min(y))#(OldMax - OldMin)  
    NewRange = (100-50)#(NewMax - NewMin) 
    scale_lst = list(map(lambda x: int(((((x - min(y)) * NewRange) / OldRange) + 50)), y)) 
    df_sort['scaled_score'] = scale_lst

    df_sort['label']=[None]*18
    df_sort['col']=[None]*18
    
    for b in range(18):
        if df_sort['deviation_score'][b] > 0.99:  #0.99
        #if c[b] >= 50:  #0.99
            df_sort['label'][b] = '1. Great Performance'
            df_sort['col'][b] = 'mediumspringgreen'  
            
        if (df_sort['deviation_score'][b] <= 0.99) & (df_sort['deviation_score'][b] > 0.98) :
        #if (c[b] <= 50) & (c[b] >= 15) :   
            df_sort['label'][b] = "2. Good Performance"
            df_sort['col'][b] = 'gold'
            
        if df_sort['deviation_score'][b] <= 0.98 :
        #if c[b] <= 0.15 :
            df_sort['label'][b] = "3. Needs Improvement"
            df_sort['col'][b] = 'tomato'
            
    if bar == 'Horizontal':
        for label, label_df in df_sort.groupby('label'):
            bars.append(go.Bar(x=label_df.scaled_score,
                           y=label_df.body_key_point ,
                           name=label,
                           text= label_df.scaled_score,
                           marker={'color': label_df.col},
                           orientation='h',
                          ))
    else :
        for label, label_df in df_sort.groupby('label'):
            bars.append(go.Bar(x=label_df.body_key_point,
                               y=label_df.scaled_score,
                               name=label,
                               marker={'color': label_df.col},
                               text= label_df.scaled_score,
                               textposition='auto',
                              ))

    fig = go.FigureWidget(data=bars)

    fig.update_layout(barmode='group',xaxis=dict(title_text="Body Part"),yaxis=dict(title_text="Scaled Deviation Score"),
                      title_text=title,title_x=0.5)
    fig.update_layout(legend_orientation="h",autosize=True,width =1760,height=530,
                        title_font_color="red",
                        legend_font_color="blue", 
                        font=dict(size=18,
                     )) 
    fig.update_layout(legend=dict(
    #yanchor="top",
    y=-.23,
    #xanchor="left",
    x=0.01
    ))
    fig.update_xaxes(tickangle=335)

    df1=pd.DataFrame(df_sort[['body_key_point','deviation_score','scaled_score','label' ]],columns=['body_key_point','deviation_score','scaled_score','label'])
    csv_file1 = 'video_report_' + str(datetime.now().strftime('%Y_%m_%d')) + '.csv'  #_%H_%M_%S
    df1.to_csv("./output/"+csv_file1,index=False)
    
    return fig,df_sort[['body_key_point','deviation_score','scaled_score','label' ]]